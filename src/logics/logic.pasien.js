import axios from 'axios';
import _ from 'lodash';
import { api } from '../constants';
import { toast } from '../utils';

/** 
 * mengambil data list pasien dari server
 * 
 * @param function
 * @param function
 * @param function 
 */
export const getDataPasienLogic = (cbSuccess, cbLoadStart, cbLoadEnd) => {
    cbLoadStart();
    axios.post(api.GET_DATA_PASIEN, { key: 'getData' }, {
        headers: {
            Accept: 'application/json',
        },
    })
        .then((response) => {
            console.log('GET DATA PASIEN: ', response);
            return response.data;
        })
        .then((res) => {
            const items = [];
            if (res !== '0' && res !== '') {
                res.forEach((item) => {
                    items.push({
                        id: item[0],
                        nama: item[1],
                        jenis_kelamin: item[2],
                        tanggal_lahir: item[3],
                    });
                });
            }
            cbSuccess(items);
        })
        .catch((err) => {
            console.log(err);
            toast('Terjadi kesalahan saat mengambil data pasien');
        })
        .then(() => cbLoadEnd());
};

/** 
 * menambah data pasien, dengan cara mengirim data ke
 * server 
 * 
 * @param  object
 * @param function
 * @param function
 * @param function
 */
export const tambahPasienLogic = (data, cbLoadStart, cbLoadEnd, cbSuccess) => {
    // validate
    if (_.isEmpty(data.nama) || _.isEmpty(data.tanggal_lahir)) {
        return toast('Lengkapi data form pasien');
    }

    // process
    cbLoadStart();
    return axios.post(api.TAMBAH_PASIEN, data, {
        headers: {
            Accept: 'application/json',
        },
    })
        .then((response) => {
            console.log('DAFTAR PASIEN: ', response);
            return response.data;
        })
        .then((res) => {
            toast(res);
            cbSuccess();
        })
        .catch((err) => {
            console.log(err);
            toast('Terjadi kesalahan saat tambah pasien');
        })
        .then(() => cbLoadEnd());
};

/** 
 * melakukan hapus data pasien dengan mengirimkan
 * id pasien ke server. Yang nantinya server akan 
 * menghapus pasien berdasarkan id pasien
 * 
 * @param number
 * @param function 
 */
export const deletePasienLogic = (idPasien, cbSuccess) => {
    axios.post(api.DELETE_PASIEN, { pasien_id: idPasien }, {
        headers: {
            Accept: 'application/json',
        },
    })
        .then((response) => {
            console.log('DELETE PASIEN: ', response);
            return response.data;
        })
        .then((res) => {
            toast(res);
            if (res !== 'Gagal menghapus data pasien') {
                cbSuccess();
            }
        })
        .catch((err) => {
            console.log(err);
            toast('Terjadi kesalahan saat hapus pasien');
        });
};

/** 
 * mendapatkan heart rate seorang pasien berdasarkan id pasien.
 * Heart rate di eksekusi setiap beberapa detik, untuk mendapatkan
 * hasil yang realtime
 * 
 * @param number
 * @param function 
 */
export const getHeartRateLogic = (pasien_id, cbSuccess) => {
    axios.get(api.GET_HEART_RATE, {
        params: {
            pasien_id,
        },
        headers: {
            Accept: 'application/json',
        },
    })
        .then((response) => {
            console.log('HEART RATE: ', response);
            return response.data;
        })
        .then((res) => {
            cbSuccess(Number(res.rate));
        })
        .catch((err) => {
            console.log(err);
        });
};

/** 
 * mendapatkan hasil kesimpulan dari pengukuran detak jantung (heart rate)
 * dengan menggunakan algoritma Triase pada bagian server.
 * Server akan mengembalikan hasil kesimpulannya ke mobile
 * 
 * @param number
 * @param number
 * @param function
 * @param function
 * @param function 
 */
export const getResultTriase = (pasien_id, umur, cbLoadStart, cbLoadEnd, cbSuccess) => {
    cbLoadStart();
    axios.post(api.GET_RESULT_TRIASE, {}, {
        headers: {
            Accept: 'application/json',
        },
        params: {
            pasien_id,
            umur,
            created_at: new Date(),
        },
    })
        .then((response) => {
            console.log('GET RESULT TRIASE: ', response);
            return response.data;
        })
        .then((res) => {
            cbSuccess(res);
        })
        .catch((err) => {
            toast('Terjadi kesalahan saat terhubung ke server');
            console.log(err);
        })
        .then(() => cbLoadEnd());
};

/** 
 * Mendapatkan data history detak pasien seorang pasien,
 * berdasarkan id pasien
 * 
 * @param number
 * @param number
 * @param function
 * @param function
 * @param function 
 */
export const getHistoryPasienLogic = (pasien_id, umur, cbLoadStart, cbLoadEnd, cbSuccess) => {
    cbLoadStart();
    axios.post(api.GET_HISTORY_PASIEN, {}, {
        headers: {
            Accept: 'application/json',
        },
        params: {
            pasien_id,
        },
    })
        .then((response) => {
            console.log('GET HISTORY PASIEN: ', response);
            return response.data;
        })
        .then((res) => {
            const items = [];
            if (res !== '0' && res !== '') {
                res.forEach((item) => {
                    items.push({
                        time: item[0],
                        bpm: Number(item[1]),
                        kondisi: item[3],
                    });
                });
            }
            cbSuccess(items);
        })
        .catch((err) => {
            toast('Terjadi kesalahan saat terhubung ke server');
            console.log(err);
        })
        .then(() => cbLoadEnd());
};
