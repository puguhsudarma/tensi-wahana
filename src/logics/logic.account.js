import { AsyncStorage } from 'react-native';
import axios from 'axios';
import RNFetchBlob from 'react-native-fetch-blob';
import _ from 'lodash';
import { api } from '../constants';
import { toast } from '../utils';

export const checkSessionLogic = (cbSuccess, cbFailed) => {
    AsyncStorage.getItem('username')
        .then((data) => {
            console.log('CHECK SESSION: ', data);
            if (data !== null) {
                cbSuccess();
            } else {
                cbFailed();
            }
        })
        .catch((err) => {
            console.log(err);
            cbFailed();
        });
};

export const loginLogic = (username, password, cbSuccess, cbLoadStart, cbLoadEnd) => {
    // validate
    if (_.isEmpty(username) || _.isEmpty(password)) {
        return toast('Isikan username dan password');
    }

    // start login
    cbLoadStart();
    return axios.post(api.LOGIN, { username, password }, {
        headers: {
            Accept: 'application/json',
        },
    })
        .then((response) => {
            console.log('LOGIN: ', response);
            return response.data;
        })
        .then((res) => {
            if (res === 'berhasil') {
                AsyncStorage.multiSet([
                    ['username', username],
                    ['password', password],
                ]);
                cbSuccess();
            } else {
                toast('Username atau password tidak ditemukan');
            }
        }).catch((err) => {
            console.log(err);
            toast('Terjadi kesalahan saat login');
        })
        .then(() => cbLoadEnd());
};

export const logoutLogic = (cbSuccess) => {
    AsyncStorage.multiRemove(['username', 'password'])
        .then(() => {
            cbSuccess();
        })
        .catch((err) => {
            console.log(err);
            toast('Terjadi kesalahan saat logout');
        });
};

export const signUpLogic = (data, cbSuccess, cbLoadStart, cbLoadEnd) => {
    // validate empty
    if (
        _.isEmpty(data.nama) ||
        _.isEmpty(data.noTelepon) ||
        _.isEmpty(data.username) ||
        _.isEmpty(data.password) ||
        _.isEmpty(data.repassword)
    ) {
        return toast('Mohon isikan semua field');
    }

    // validate not match password
    if (data.password !== data.repassword) {
        return toast('Passowrd konfirmasi tidak sama');
    }

    // signup process
    cbLoadStart();
    return axios.post(api.SIGNUP, data, {
        headers: {
            Accept: 'application/json',
        },
    })
        .then((response) => {
            console.log('SIGNUP: ', response);
            return response.data;
        })
        .then((res) => {
            if (res === 'Berhasil') {
                cbSuccess();
            } else {
                toast('Gagal saat pendaftaran');
            }
        }).catch((err) => {
            console.log(err);
            toast('Terjadi kesalahan saat pendaftaran');
        })
        .then(() => cbLoadEnd());
};

export const getDataPerawatLogic = (cbSuccess) => {
    AsyncStorage.multiGet(['username', 'password'])
        .then(([username, password]) => {
            return axios.post(
                api.GET_DATA_PERAWAT,
                {
                    username: username[1],
                    password: password[1],
                },
                {
                    headers: {
                        Accept: 'application/json',
                    },
                },
            );
        })
        .then((response) => {
            console.log('GET DATA PERAWAT: ', response);
            return response.data;
        })
        .then((res) => {
            let dataPerawat = {};
            if (res !== null) {
                dataPerawat = {
                    ...res,
                    path: `${api.URL}/${res.path}`,
                };
            }
            cbSuccess(dataPerawat);
        })
        .catch((err) => {
            console.log(err);
            toast('Terjadi kesalahan saat load data perawat');
        });
};

export const updateProfileLogic = (data, cbLoadStart, cbLoadEnd) => {
    cbLoadStart();
    axios.post(api.UPDATE_PROFILE, data, {
        headers: {
            Accept: 'application/json',
        },
    })
        .then(response => response.data)
        .then(() => {
            toast('Profil berhasil diupdate');
        })
        .catch((err) => {
            console.log(err);
            toast('Terjadi kesalahan saat update profile');
        })
        .then(() => cbLoadEnd());
};

export const uploadImageProfileLogic = (idPerawat, imagesUri, cbLoadStart, cbLoadEnd) => {
    cbLoadStart();
    RNFetchBlob.fetch(
        'POST',
        api.UPLOAD_PHOTO,
        { 'Content-Type': 'multipart/form-data' },
        [{
            name: 'image',
            type: 'image/png',
            filename: `${idPerawat}-${new Date().getTime()}`,
            data: RNFetchBlob.wrap(imagesUri),
        }],
    )
        .then(response => JSON.stringify(response))
        .then((resp) => {
            toast(resp);
        }).catch((err) => {
            console.log(err);
            toast('Terjadi kesalahan saat upload foto profil');
        })
        .then(() => cbLoadEnd());
};
