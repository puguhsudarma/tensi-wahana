export const kondisiPasien = (umur, bpm) => {
    let kondisi = '';

    if (umur >= 0 && umur <= 1) {
        if (bpm < 130) {
            kondisi = 'Bradikardi';
        } else {
            kondisi = 'Takikardi';
        }
    } else if (umur >= 1 && umur <= 3) {
        if (bpm < 100) {
            kondisi = 'Bradikardi';
        } else {
            kondisi = 'Takikardi';
        }
    } else if (umur >= 3 && umur <= 12) {
        if (bpm < 90) {
            kondisi = 'Bradikardi';
        } else {
            kondisi = 'Takikardi';
        }
    } else if (umur >= 12 && umur <= 18) {
        if (bpm < 80) {
            kondisi = 'Bradikardi';
        } else {
            kondisi = 'Takikardi';
        }
    } else {
        // eslint-disable-next-line
        if (bpm < 60) {
            kondisi = 'Bradikardi';
        } else {
            kondisi = 'Takikardi';
        }
    }

    return kondisi;
};

export const kategoriUser = (umur) => {
    let kategori = '';

    if (umur >= 0 && umur <= 3) {
        kategori = 'Bayi';
    } else if (umur >= 3 && umur <= 12) {
        kategori = 'Anak - anak';
    } else if (umur >= 12 && umur <= 18) {
        kategori = 'Remaja';
    } else {
        kategori = 'Dewasa';
    }

    return kategori;
};
