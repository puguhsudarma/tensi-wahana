import { createStackNavigator } from 'react-navigation';
import { configureTachyons } from './utils';
import SplashScreen from './modules/SplashScreen';
import Login from './modules/Login';
import SignUp from './modules/SignUp';
import Dashboard from './modules/Dashboard';
import TambahPasien from './modules/TambahPasien';
import UkurPasien from './modules/UkurPasien';
import HistoryPasien from './modules/HistoryPasien';
import Profile from './modules/Profile';

const TensiApp = createStackNavigator(
    {
        SplashScreen: { screen: SplashScreen },
        Login: { screen: Login },
        SignUp: { screen: SignUp },
        Dashboard: { screen: Dashboard },
        TambahPasien: { screen: TambahPasien },
        UkurPasien: { screen: UkurPasien },
        HistoryPasien: { screen: HistoryPasien },
        Profile: { screen: Profile },
    },
    {
        initialRouteName: 'SplashScreen',
        navigationOptions: {
            header: null,
        },
    },
);

// disable warning message in mobile
console.disableYellowBox = true;

// register Tachyons
configureTachyons();

export default TensiApp;
