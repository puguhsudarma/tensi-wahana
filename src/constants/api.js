export const URL = 'https://tensi-wahana.herokuapp.com';

export const LOGIN = `${URL}/login.php`;
export const SIGNUP = `${URL}/tambah_perawat.php`;
export const GET_DATA_PERAWAT = `${URL}/get_data_perawat.php`;
export const UPDATE_PROFILE = `${URL}/update_profile.php`;
export const UPLOAD_PHOTO = `${URL}/upload_foto.php`;
export const GET_DATA_PASIEN = `${URL}/get_data_pasien.php`;
export const TAMBAH_PASIEN = `${URL}/tambah_pasien.php`;
export const DELETE_PASIEN = `${URL}/delete_pasien.php`;
export const GET_HEART_RATE = `${URL}/get_heartrate.php`;
export const GET_HEART_RATE_PASIEN = `${URL}/get_heart_rate_pasien.php`;
export const GET_RESULT_TRIASE = `${URL}/get_result_triase.php`;
export const GET_HISTORY_PASIEN = `${URL}/get_history_pasien.php`;
