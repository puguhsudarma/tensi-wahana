import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    Dimensions,
    ActivityIndicator,
    SafeAreaView,
} from 'react-native';
import _ from 'lodash';
import { mean } from 'mathjs';
import PropTypes from 'prop-types';
import { differenceInYears } from 'date-fns';
import { styles as s } from 'react-native-style-tachyons';
import { LineChart, YAxis } from 'react-native-svg-charts';
import { Content, Icon } from 'native-base';
import { getHeartRateLogic, getResultTriase } from '../logics/logic.pasien';
import { ms } from '../utils';
import { colors } from '../constants';
import { kategoriUser } from '../logics/_utils';

const { width, height } = Dimensions.get('window');

const ItemHasil = ({ label, value }) => {
    return (
        <View style={[s.flx_row]}>
            <View style={{ width: ms(100) }}>
                <Text style={{
                    color: 'white',
                    fontWeight: '500',
                }}>
                    {`${label} : `}
                </Text>
            </View>
            <Text style={{ color: 'white' }}>
                {value}
            </Text>
        </View>
    );
};

ItemHasil.propTypes = {
    label: PropTypes.string.isRequired,
    value: PropTypes.oneOfType([PropTypes.number, PropTypes.string]).isRequired,
};

class UkurPasien extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
            dataRate: [],
            bpm: 0,
            maxBpm: 0,
            btnAction: 'Start',
            isStart: false,
            kondisi: '',
            item: {
                id: '',
                nama: '',
                umur: '',
                tanggal_lahir: '',
                kategori: '',
                jenis_kelamin: '',
            },
            loading: false,
        };

        this._timerId = null;
        this._handleBack = this._handleBack.bind(this);
        this._getDataHeartRate = this._getDataHeartRate.bind(this);
        this._handleClickFetchRate = this._handleClickFetchRate.bind(this);
        this._handleResultTriase = this._handleResultTriase.bind(this);
        this._handleHistoryPasien = this._handleHistoryPasien.bind(this);
    }

    componentWillUnmount() {
        clearInterval(this._timerId);
    }

    _handleBack() {
        if (!this.state.isStart && !this.state.loading) {
            this.props.navigation.goBack();
        }
    }

    _getDataHeartRate() {
        const item = this.props.navigation.getParam('item', this.state.item);
        getHeartRateLogic(item.id, (rate) => {
            if (rate > 0) {
                const maxBpm = this.state.maxBpm < rate ? rate : this.state.maxBpm;
                const potong = this.state.data.slice(1);
                const joined = potong.concat(rate);
                this.setState({ data: joined, bpm: rate, maxBpm });
            } else {
                const potong = this.state.data.slice(1);
                const joined = potong.concat(0);
                this.setState({ data: joined, bpm: 0 });
            }
            this.setState({ dataRate: [...this.state.dataRate, rate] });
        });
    }

    _handleClickFetchRate() {
        this.setState({ isStart: !this.state.isStart }, () => {
            if (this.state.isStart) {
                this.setState({ btnAction: 'Stop', dataRate: [] });
                this._getDataHeartRate(); // init
                this._timerId = setInterval(this._getDataHeartRate, 1000);
            } else {
                clearInterval(this._timerId);
                this.setState({ btnAction: 'Start' });
                this._handleResultTriase();
            }
        });
    }

    _handleResultTriase() {
        const item = this.props.navigation.getParam('item', this.state.item);
        const umur = differenceInYears(new Date(), new Date(item.tanggal_lahir));
        getResultTriase(
            item.id,
            umur,
            () => this.setState({ loading: true }),
            () => this.setState({ loading: false }),
            kondisi => this.setState({
                kondisi,
                item: {
                    ...item,
                    umur,
                    kategori: kategoriUser(umur),
                },
            }),
        );
    }

    _handleHistoryPasien() {
        const item = this.props.navigation.getParam('item', this.state.item);
        this.props.navigation.navigate('HistoryPasien', {
            item,
        });
    }

    render() {
        const contentInset = { top: height - 570, bottom: height - 570 };
        const { kondisi, item, loading, dataRate } = this.state;
        return (
            <SafeAreaView style={[s.flx_i, { backgroundColor: '#353E4F' }]}>
                <View style={[s.flx_i]}>
                    <View style={[s.flx_row, s.jcc, {
                        height: 80,
                        borderBottomWidth: 1,
                        borderBottomColor: colors.COOL_GREY_1,
                    }]}>
                        <View style={[s.absolute, s.top_0, s.bottom_0, s.left_0, s.jcc, s.aic, {
                            width: ms(50),
                        }]}>
                            <TouchableOpacity onPress={this._handleBack}>
                                <Icon style={{ color: colors.WHITE, fontSize: 30 }} name="ios-arrow-back" />
                            </TouchableOpacity>
                        </View>

                        <View style={[s.flx_i, s.jcc, s.aic]}>
                            <Text style={{ color: colors.WHITE, fontSize: 18 }}>Heart Rate</Text>
                        </View>

                        <View style={[s.absolute, s.top_0, s.bottom_0, s.right_0, s.jcc, s.aic, {
                            marginRight: ms(10),
                        }]}>
                            <TouchableOpacity onPress={this._handleHistoryPasien}>
                                <View style={{
                                    backgroundColor: colors.WHITE,
                                    padding: ms(10),
                                    borderRadius: ms(5),
                                }}>
                                    <Text style={{ color: colors.BLACK }}>
                                        {'History'}
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>

                    <Content>
                        <View style={{ borderColor: 'white', borderWidth: 4, height: 150, width: 150, borderRadius: 150, alignItems: 'center', alignSelf: 'center', paddingTop: 1, marginTop: 20 }}>
                            <View style={{ borderColor: 'white', borderWidth: 1, height: 140, width: 140, borderRadius: 150, flexDirection: 'row', paddingTop: 40, paddingLeft: 15 }}>
                                <View style={{ position: 'absolute', top: 45, left: 13, width: 70 }}>
                                    <Text style={{ color: 'white', fontSize: 40, textAlign: 'right' }}>{this.state.bpm}</Text>
                                </View>
                                <View style={{ position: 'absolute', top: 50, right: 9 }}>
                                    <Text style={{ color: 'white', fontSize: 20 }}>bpm</Text>
                                </View>
                                <View style={{ position: 'absolute', top: 20, left: 60 }}>
                                    <Icon name="ios-heart" style={{ color: 'white' }} />
                                </View>
                            </View>
                        </View>

                        <View style={{ height: 40, width: width - 100, alignSelf: 'center', borderRadius: 40, marginTop: 15, borderWidth: 1, borderColor: 'white' }}>
                            <TouchableOpacity
                                onPress={this._handleClickFetchRate}
                                style={{ alignItems: 'center', width: width - 100, height: 40 }}
                            >
                                <Text style={{ color: 'white', fontSize: 20, marginTop: 5 }}>
                                    {this.state.btnAction}
                                </Text>
                            </TouchableOpacity>
                        </View>

                        <View style={{ width, flexDirection: 'row', marginTop: 10 }}>
                            <YAxis
                                data={this.state.data}
                                contentInset={contentInset}
                                svg={{
                                    fill: 'white',
                                    fontSize: 10,
                                }}
                                formatLabel={value => `${value} bpm`}
                            />
                            <LineChart
                                style={{ width: width - 80, height: height / 2 }}
                                data={this.state.data}
                                svg={{ stroke: 'white' }}
                                animate
                                contentInset={{ top: 20, bottom: 20 }}
                            />
                        </View>

                        <View style={{ marginTop: 20, padding: 10 }}>
                            <View style={[s.jcc, s.aic, {
                                borderBottomColor: colors.COOL_GREY_1,
                                borderBottomWidth: ms(1),
                                paddingBottom: ms(2),
                                marginBottom: ms(5),
                            }]}>
                                <Text style={{ color: 'white', fontSize: ms(16) }}>HASIL PENGUKURAN</Text>
                            </View>

                            <View>
                                <ItemHasil label="ID Pasien" value={item.id} />
                                <ItemHasil label="Nama" value={item.nama} />
                                <ItemHasil label="Umur" value={item.umur} />
                                <ItemHasil label="Kategori" value={item.kategori} />
                                <ItemHasil label="Tanggal Lahir" value={item.tanggal_lahir} />
                                <ItemHasil label="Jenis Kelamin" value={item.jenis_kelamin} />
                                <View style={{ height: ms(20) }} />
                                <ItemHasil label="Avg. BPM" value={!_.isEmpty(dataRate) ? mean(dataRate) : 0} />
                                <ItemHasil label="Kondisi" value={kondisi} />
                            </View>

                            {
                                loading &&
                                <View style={[s.absolute, s.top_0, s.bottom_0, s.left_0, s.right_0, s.jcc, s.aic, {
                                    backgroundColor: 'rgba(0, 0, 0, 0.8)',
                                }]}>
                                    <ActivityIndicator size="large" />
                                    <Text style={{ color: colors.WHITE }}>
                                        {'Mohon tunggu, memuat hasil tes...'}
                                    </Text>
                                </View>
                            }
                        </View>
                    </Content>
                </View>
            </SafeAreaView>
        );
    }
}

UkurPasien.propTypes = {
    navigation: PropTypes.shape().isRequired,
};

export default UkurPasien;
