import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    Dimensions,
    FlatList,
    RefreshControl,
    ScrollView,
    SafeAreaView,
} from 'react-native';
import { differenceInYears } from 'date-fns';
import PropTypes from 'prop-types';
import { Icon } from 'native-base';
import _ from 'lodash';
import { styles as s } from 'react-native-style-tachyons';
import { ms } from '../utils';
import { colors } from '../constants';
import { getHistoryPasienLogic } from '../logics/logic.pasien';

const { width } = Dimensions.get('window');

class HistoryPasien extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            data: [],
            item: {
                id: '',
                nama: '',
                umur: '',
                tanggal_lahir: '',
                jenis_kelamin: '',
            },
        };

        this._handleBack = this._handleBack.bind(this);
        this._handleGetHistory = this._handleGetHistory.bind(this);
    }

    componentDidMount() {
        this._handleGetHistory();
    }

    _handleGetHistory() {
        const item = this.props.navigation.getParam('item', this.state.item);
        const umur = differenceInYears(new Date(), new Date(item.tanggal_lahir));
        getHistoryPasienLogic(
            item.id,
            umur,
            () => this.setState({ loading: true }),
            () => this.setState({ loading: false }),
            data => this.setState({ data }),
        );
    }

    _handleBack() {
        this.props.navigation.goBack();
    }

    render() {
        return (
            <SafeAreaView style={[s.flx_i, { backgroundColor: '#353E4F' }]}>
                <View style={[s.flx_row, s.jcc, {
                    height: 80,
                    borderBottomWidth: 1,
                    borderBottomColor: colors.COOL_GREY_1,
                }]}>
                    <View style={[s.absolute, s.top_0, s.bottom_0, s.left_0, s.jcc, s.aic, {
                        width: ms(50),
                    }]}>
                        <TouchableOpacity onPress={this._handleBack}>
                            <Icon style={{ color: colors.WHITE, fontSize: 30 }} name="ios-arrow-back" />
                        </TouchableOpacity>
                    </View>

                    <View style={[s.flx_i, s.jcc, s.aic]}>
                        <Text style={{ color: colors.WHITE, fontSize: 18 }}>History Pasien</Text>
                    </View>
                </View>

                <View style={[s.flx_i, s.jcc, s.aic]}>
                    {
                        !_.isEmpty(this.state.data) ?
                            <FlatList
                                data={this.state.data}
                                extraData={this.props}
                                initialNumToRender={7}
                                keyExtractor={(item, index) => index.toFixed()}
                                refreshControl={
                                    <RefreshControl
                                        onRefresh={this._handleGetHistory}
                                        refreshing={this.state.loading}
                                    />
                                }
                                renderItem={({ item }) => {
                                    return (
                                        <View style={{
                                            marginVertical: ms(5),
                                            paddingVertical: ms(5),
                                            paddingHorizontal: ms(10),
                                            width: width * 0.97,
                                            backgroundColor: 'rgba(255,255,255,0.3)',
                                        }}>
                                            <Text style={{ color: 'white', fontSize: ms(20), fontWeight: '300' }}>{`${item.bpm.toFixed(0)} BPM`}</Text>
                                            <Text style={{ color: 'white', fontSize: ms(14) }}>{item.kondisi}</Text>
                                            <Text style={{ color: 'white', fontSize: ms(12) }}>{item.time}</Text>
                                        </View>
                                    );
                                }}
                            />

                            :

                            <ScrollView
                                refreshControl={
                                    <RefreshControl
                                        onRefresh={this._handleGetHistory}
                                        refreshing={this.state.loading}
                                    />
                                }
                                contentContainerStyle={[s.flx_i, s.jcc, s.aic]}
                            >
                                <Text style={{ color: colors.WHITE, fontSize: ms(16) }}>
                                    {'Tidak ada data history pasien...'}
                                </Text>
                            </ScrollView>
                    }
                </View>
            </SafeAreaView>
        );
    }
}

HistoryPasien.propTypes = {
    navigation: PropTypes.shape().isRequired,
};

export default HistoryPasien;
