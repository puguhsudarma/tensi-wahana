import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    TouchableWithoutFeedback,
    TextInput,
    Image,
    Modal,
    Dimensions,
    ActivityIndicator,
} from 'react-native';
import _ from 'lodash';
import { styles as s } from 'react-native-style-tachyons';
import {
    Content,
    Button,
    Icon,
} from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import ImagePicker from 'react-native-image-crop-picker';
import { getDataPerawatLogic, updateProfileLogic, uploadImageProfileLogic } from '../logics/logic.account';
import { toast, ms } from '../utils';

const { width, height } = Dimensions.get('window');

class Profile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            nama: '',
            username: '',
            nomor_telepon: '',
            imagesUri: '',
            tempUri: '',
            id_perawat: '',
            modalVisible: false,
            loading: false,
        };

        this._handleGetDataPerawat = this._handleGetDataPerawat.bind(this);
        this._selectImage = this._selectImage.bind(this);
        this._handleUpdateProfile = this._handleUpdateProfile.bind(this);
    }

    componentDidMount() {
        this._handleGetDataPerawat();
    }

    _handleGetDataPerawat() {
        getDataPerawatLogic((res) => {
            this.setState({
                nama: res.nama,
                username: res.username,
                nomor_telepon: res.no_telepon,
                imagesUri: res.path,
                tempUri: res.path,
                id_perawat: res.id_perawat,
            });
        });
    }

    _selectImage() {
        ImagePicker.openPicker({
            width: 400,
            height: 400,
            cropping: true,
        })
            .then((image) => {
                this.setState({
                    imagesUri: image.path,
                    modalVisible: false,
                });
            })
            .catch((err) => {
                console.log(err);
                toast('Terjadi kesalahan saat memilih foto');
            });
    }

    _handleUpdateProfile() {
        const {
            imagesUri,
            tempUri,
            modalVisible,
            loading,
            ...profile
        } = this.state;

        updateProfileLogic(
            profile,
            () => this.setState({ loading: true }),
            () => this.setState({ loading: false }),
        );

        if (!_.isEqual(imagesUri, tempUri)) {
            uploadImageProfileLogic(
                profile.id_perawat,
                imagesUri,
                () => this.setState({ loading: true }),
                () => this.setState({ loading: false }),
            );
        }
    }

    render() {
        return (
            <View style={[s.flx_i, s.aic, { backgroundColor: '#353E4F' }]}>

                {/* Header Bar */}
                <View style={[s.flx_row, {
                    width,
                    height: 80,
                    backgroundColor: '#353E4F',
                    borderBottomWidth: 1,
                    borderBottomColor: 'grey',
                }]}>
                    <View style={{ width: 50, height: 80 }}>
                        <Button
                            onPress={() => { }}
                            style={{ zIndex: 1, position: 'absolute', marginTop: '40%', marginLeft: -2 }}
                            transparent
                            light
                        >
                            <Icon style={{ color: 'white', fontSize: 30 }} name="ios-arrow-back" />
                        </Button>
                    </View>
                    <View style={{ width: width - 50, height: 80, paddingLeft: width / 3.5, paddingTop: '8%' }}>
                        <Text style={{ color: 'white', fontSize: 18 }}>
                            {'Profile'}
                        </Text>
                    </View>
                </View>

                {/* Modal Memilih Photo */}
                <Modal animationType="fade" transparent visible={this.state.modalVisible} onRequestClose={() => this.setState({ modalVisible: false })}>
                    <TouchableWithoutFeedback onPress={() => this.setState({ modalVisible: false })}>
                        <View style={{ height, width, backgroundColor: 'rgba(51,44,43,0.5)' }}>
                            <TouchableWithoutFeedback>
                                <View style={{ backgroundColor: 'white', height: height / 5, width: width - 100, borderRadius: 5, alignSelf: 'center', marginTop: height / 2.5 }}>
                                    <View style={{ height: 35, width: width - 100, backgroundColor: '#353E4F', borderTopLeftRadius: 5, borderTopRightRadius: 5 }}>
                                        <Text style={{ color: 'white', fontSize: 18, textAlign: 'center', marginTop: 5 }}>Pilih foto dari</Text>
                                    </View>
                                    <TouchableOpacity onPress={() => this.openCamera()}>
                                        <View style={{ width: width - 100, height: 40 }}>
                                            <Text style={{ color: 'black', fontSize: 15, marginLeft: 5, marginTop: 12 }}>Kamera</Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this.selectImage()}>
                                        <View style={{ width: width - 100, height: 40 }}>
                                            <Text style={{ color: 'black', fontSize: 15, marginLeft: 5, marginTop: 12 }}>Library</Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                            </TouchableWithoutFeedback>
                        </View>
                    </TouchableWithoutFeedback>
                </Modal>

                {/* Form Data Perawat */}
                <Content>
                    {/* Photo Profile */}
                    <View style={{ paddingLeft: (width / 2) - 70 }}>
                        <View style={{ height: 80, width: 80, borderRadius: 40, backgroundColor: 'gray', marginTop: 20 }}>
                            <TouchableOpacity style={{ height: 80, width: 80, borderRadius: 40, backgroundColor: 'gray' }} onPress={() => this.setState({ modalVisible: true })}>
                                <View style={{ height: 80, width: 80, borderRadius: 40, position: 'absolute' }}>
                                    <Icon name="person" style={{ color: 'white', textAlign: 'center', marginTop: 18, fontSize: 50 }} />
                                </View>
                                <Image style={{ alignSelf: 'center', height: 80, width: 80, borderRadius: 40 }} source={{ uri: this.state.imagesUri }} />
                            </TouchableOpacity>
                        </View>
                    </View>


                    <View style={{ width: width - 100, marginTop: 20 }}>
                        <Text style={{ color: 'white', fontSize: 15, marginTop: '3%', marginRight: '5%' }}>Nama</Text>
                    </View>
                    <TextInput value={this.state.nama} onChangeText={nama => this.setState({ nama })} underlineColorAndroid="transparent" style={{ textAlign: 'center', width: width - 50, height: 45, backgroundColor: 'white', borderRadius: 25, marginTop: '1%' }} />

                    <View style={{ width: width - 100 }}>
                        <Text style={{ color: 'white', fontSize: 15, marginTop: '3%', marginRight: '5%' }}>No. Telepon</Text>
                    </View>
                    <TextInput value={this.state.nomor_telepon} onChangeText={nomor_telepon => this.setState({ nomor_telepon })} underlineColorAndroid="transparent" style={{ textAlign: 'center', width: width - 50, height: 45, backgroundColor: 'white', borderRadius: 25, marginTop: '1%' }} />

                    <View style={{ width: width - 100 }}>
                        <Text style={{ color: 'white', fontSize: 15, marginTop: '3%', marginRight: '5%' }}>Username</Text>
                    </View>
                    <TextInput value={this.state.username} onChangeText={username => this.setState({ username })} underlineColorAndroid="transparent" style={{ textAlign: 'center', width: width - 50, height: 45, backgroundColor: 'white', borderRadius: 25, marginTop: '1%' }} />


                    {/* tombol tambah pasien */}
                    <TouchableOpacity style={{ marginTop: 30 }} onPress={this._handleUpdateProfile}>
                        <LinearGradient
                            start={{ x: 0.0, y: 0.25 }}
                            end={{ x: 0.5, y: 1.0 }}
                            locations={[0, 1.0, 0.6]}
                            colors={['#70D9A0', '#59C7E8']}
                            style={{ width: width - 50, height: 45, borderRadius: 25, alignItems: 'center', paddingTop: 8 }}>
                            <Text style={{ fontSize: 18, color: 'black' }}>
                                {'Update Data'}
                            </Text>

                            {
                                this.state.loading &&
                                <View style={[s.absolute, s.top_0, s.bottom_0, s.right_0, {
                                    marginRight: ms(10),
                                }]}>
                                    <ActivityIndicator />
                                </View>
                            }
                        </LinearGradient>
                    </TouchableOpacity>
                </Content>
            </View>
        );
    }
}

export default Profile;
