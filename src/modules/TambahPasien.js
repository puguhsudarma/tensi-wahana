import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    TextInput,
    ActivityIndicator,
    Dimensions,
    DatePickerAndroid,
    Keyboard,
    TouchableHighlight,
    Picker,
} from 'react-native';
import {
    Button,
    Icon,
} from 'native-base';
import PropTypes from 'prop-types';
import { styles as s } from 'react-native-style-tachyons';
import LinearGradient from 'react-native-linear-gradient';
import { tambahPasienLogic } from '../logics/logic.pasien';
import { ms, toast } from '../utils';

const { width } = Dimensions.get('window');

class TambahPasien extends Component {
    constructor(props) {
        super(props);

        this.state = {
            nama: '',
            tanggal_lahir: '',
            jenis_kelamin: 'laki-laki',
            loading: false,
        };

        this._handleBack = this._handleBack.bind(this);
        this._handleTambahPasien = this._handleTambahPasien.bind(this);
        this._androidDatePicker = this._androidDatePicker.bind(this);
    }

    _handleBack() {
        const close = this.props.navigation.getParam('close', () => { });
        const getDataPasien = this.props.navigation.getParam('getDataPasien', () => { });

        // execute callback
        this.props.navigation.goBack();
        getDataPasien();
        close();
    }

    _handleTambahPasien() {
        const { loading, ...profile } = this.state;
        tambahPasienLogic(
            profile,
            () => this.setState({ loading: true }),
            () => this.setState({ loading: false }),
            this._handleBack,
        );
    }

    _androidDatePicker() {
        Keyboard.dismiss();
        DatePickerAndroid.open({
            // Use `new Date()` for current date.
            // May 25 2020. Month 0 is January.
            date: new Date('2000-1-1'),
        })
            .then(({ action, day, month, year }) => {
                if (action !== DatePickerAndroid.dismissedAction) {
                    // Selected year, month (0-11), day
                    this.setState({ tanggal_lahir: `${year}-${month + 1 < 10 ? `0${month + 1}` : month + 1}-${day < 10 ? `0${day}` : day}` });
                }
            })
            .catch(() => {
                toast('Terjadi kesalahan saat memilih tanggal');
            });
    }

    render() {
        return (
            <View style={[s.flx_i, s.aic, { backgroundColor: '#353E4F' }]}>
                {/* Header Bar */}
                <View style={{ width, height: 80, backgroundColor: '#353E4F', borderBottomWidth: 1, borderBottomColor: 'grey', flexDirection: 'row' }}>
                    <View style={{ width: 50, height: 80 }}>
                        <Button
                            transparent
                            light
                            style={{ zIndex: 1, position: 'absolute', marginTop: '40%', marginLeft: -2 }}
                            onPress={this._handleBack}
                        >
                            <Icon style={{ color: 'white', fontSize: 30 }} name="ios-arrow-back" />
                        </Button>
                    </View>
                    <View style={{ width: width - 50, height: 80, paddingLeft: (width / 4.5), paddingTop: '8%' }}>
                        <Text style={{ color: 'white', fontSize: 18 }}>Tambah Pasien</Text>
                    </View>
                </View>

                {/* Form Data Pasien */}
                <View style={{ width: width - 100 }}>
                    <Text style={{ color: 'white', fontSize: 15, marginTop: '3%', marginRight: '5%' }}>Nama pasien</Text>
                </View>
                <TextInput value={this.state.nama} placeholder="Nama Pasien" onChangeText={nama => this.setState({ nama })} underlineColorAndroid="transparent" style={{ textAlign: 'center', width: width - 50, height: 45, backgroundColor: 'white', borderRadius: 25, marginTop: '1%' }} />

                <View style={{ width: width - 100 }}>
                    <Text style={{ color: 'white', fontSize: 15, marginTop: '3%', marginRight: '5%' }}>Tanggal lahir</Text>
                </View>
                <TouchableHighlight onPress={this._androidDatePicker}>
                    <TextInput editable={false} value={this.state.tanggal_lahir} placeholder="Tanggal Lahir" underlineColorAndroid="transparent" style={{ textAlign: 'center', width: width - 50, height: 45, backgroundColor: 'white', borderRadius: 25, marginTop: '1%' }} />
                </TouchableHighlight>

                <View style={{ width: width - 100 }}>
                    <Text style={{ color: 'white', fontSize: 15, marginTop: '3%', marginRight: '5%' }}>Jenis Kelamin</Text>
                </View>
                <View style={{ width: width - 50, height: 45, marginLeft: '5%', marginTop: '1%', marginRight: '5%', borderWidth: 1, borderRadius: 25, borderColor: 'white', backgroundColor: 'white' }}>
                    <Picker
                        selectedValue={this.state.jenis_kelamin}
                        onValueChange={jenis_kelamin => this.setState({ jenis_kelamin })}
                    >
                        <Picker.Item label="Laki - Laki" value="laki-laki" style={{ fontSize: 10 }} />
                        <Picker.Item label="Perempuan" value="perempuan" style={{ fontSize: 10 }} />
                    </Picker >
                </View>

                {/* Tombol Tambah Pasien */}
                <TouchableOpacity
                    style={{ marginTop: 30 }}
                    onPress={this._handleTambahPasien}
                    disabled={this.state.loading}
                >
                    <LinearGradient
                        start={{ x: 0.0, y: 0.25 }}
                        end={{ x: 0.5, y: 1.0 }}
                        locations={[0, 0.6]}
                        colors={['#70D9A0', '#59C7E8']}
                        style={{ width: width - 50, height: 45, borderRadius: 25, alignItems: 'center', paddingTop: 8 }}
                    >
                        <Text style={{ fontSize: 18, color: 'black' }}>
                            {'Tambah pasien'}
                        </Text>

                        {
                            this.state.loading &&
                            <View style={[s.absolute, s.top_0, s.bottom_0, s.right_0, s.jcc, s.aic, {
                                marginRight: ms(10),
                            }]}>
                                <ActivityIndicator color="#fff" size="large" />
                            </View>
                        }
                    </LinearGradient>
                </TouchableOpacity>
            </View>
        );
    }
}

TambahPasien.propTypes = {
    navigation: PropTypes.shape().isRequired,
};

export default TambahPasien;
