import React, { Component } from 'react';
import {
    View,
    Image,
    StatusBar,
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import PropTypes from 'prop-types';
import { styles as s } from 'react-native-style-tachyons';
import { checkSessionLogic } from '../logics/logic.account';

class SplashScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {

        };

        this._checkSession = this._checkSession.bind(this);
    }

    componentDidMount() {
        this._checkSession();
    }

    _checkSession() {
        const resetActionDashboard = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Dashboard' })],
        });
        const resetActionLogin = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Login' })],
        });
        setTimeout(() => {
            checkSessionLogic(
                () => this.props.navigation.dispatch(resetActionDashboard),
                () => this.props.navigation.dispatch(resetActionLogin),
            );
        }, 3000);
    }

    render() {
        return (
            <View style={[s.flx_i, s.aic, s.jcc, {
                backgroundColor: '#353E4F',
            }]}>
                <StatusBar
                    backgroundColor="rgba(16, 19, 22,0.05)"
                    translucent
                />
                <Image
                    source={require('../images/logo.png')}
                    style={{ height: 200, width: 200 }}
                />
            </View>
        );
    }
}

SplashScreen.propTypes = {
    navigation: PropTypes.shape().isRequired,
};

export default SplashScreen;
