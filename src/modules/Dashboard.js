import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    Image,
    Dimensions,
    FlatList,
    RefreshControl,
    ScrollView,
    AsyncStorage,
} from 'react-native';
import PropTypes from 'prop-types';
import {
    Container,
    Button,
    Icon,
    Item,
} from 'native-base';
import _ from 'lodash';
import { StackActions, NavigationActions } from 'react-navigation';
import { styles as s } from 'react-native-style-tachyons';
import Drawer from 'react-native-drawer';
import { getDataPasienLogic, deletePasienLogic } from '../logics/logic.pasien';
import { logoutLogic } from '../logics/logic.account';
import { ms } from '../utils';
import { colors } from '../constants';

const { width } = Dimensions.get('window');

class Dashboard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [],
            username: '',
            imagesUri: require('../images/avatar5.png'),
            loading: false,
        };

        this._handleLogout = this._handleLogout.bind(this);
        this._handleGetDataPasien = this._handleGetDataPasien.bind(this);
        this._handleGetDataPerawat = this._handleGetDataPerawat.bind(this);
        this._goToTambahPasien = this._goToTambahPasien.bind(this);
        this._goToUkurPasien = this._goToUkurPasien.bind(this);
        this._goToProfile = this._goToProfile.bind(this);
        this._handleDeletePasien = this._handleDeletePasien.bind(this);
        this._handleOpenControlPanel = this._handleOpenControlPanel.bind(this);
    }

    componentDidMount() {
        this._handleGetDataPasien();
        this._handleGetDataPerawat();
    }

    _handleLogout() {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: 'Login' })],
        });
        logoutLogic(() => this.props.navigation.dispatch(resetAction));
    }

    _handleGetDataPasien() {
        getDataPasienLogic(
            data => this.setState({ data }),
            () => this.setState({ loading: true }),
            () => this.setState({ loading: false }),
        );
    }

    _handleGetDataPerawat() {
        AsyncStorage.getItem('username')
            .then((username) => {
                if (username) {
                    this.setState({ username: username.toUpperCase() });
                }
            })
            .catch((err) => {
                console.log('GET USERNAME: ', err);
            });
    }

    _goToTambahPasien() {
        this.props.navigation.navigate('TambahPasien', {
            close: this._drawer.close,
            getDataPasien: this._handleGetDataPasien,
        });
    }

    _goToUkurPasien(item) {
        this.props.navigation.navigate('UkurPasien', {
            item,
        });
    }

    _goToProfile() {
        this.props.navigation.navigate('Profile');
    }

    _handleDeletePasien(id) {
        deletePasienLogic(id, () => {
            this.setState({
                data: this.state.data.filter(item => item.id !== id),
            });
        });
    }

    _handleOpenControlPanel() {
        this._drawer.open();
    }

    render() {
        return (
            <Drawer
                ref={(ref) => { this._drawer = ref; }}
                type="static"
                tapToClose
                openDrawerOffset={0.2}
                panCloseMask={0.2}
                closedDrawerOffset={-3}
                styles={{ width, shadowColor: '#000000', shadowOpacity: 0.8, shadowRadius: 3 }}
                tweenHandler={ratio => ({ main: { opacity: (2 - ratio) / 2 } })}
                content={
                    <View>
                        <View style={{ alignItems: 'center', height: 150, justifyContent: 'center', backgroundColor: '#353E4F' }}>
                            <View style={{ height: 80, width: 80, borderRadius: 40, backgroundColor: 'gray', marginTop: 20, marginBottom: ms(10) }}>
                                <TouchableOpacity
                                    style={{ height: 80, width: 80, borderRadius: 40, backgroundColor: 'gray' }}
                                    onPress={this._goToProfile}
                                    disabled
                                >
                                    <Image style={{ alignSelf: 'center', height: 80, width: 80, borderRadius: 40 }} source={this.state.imagesUri} />
                                </TouchableOpacity>
                            </View>

                            <Text style={{ fontSize: ms(16), color: '#fff' }}>
                                {this.state.username || 'USERNAME'}
                            </Text>
                        </View>

                        <Container>
                            <Item style={{ height: 50, paddingLeft: 10 }} onPress={this._goToTambahPasien}>
                                <Icon name="ios-add" style={{ color: '#353E4F' }} />
                                <Text style={{ marginLeft: 10, color: 'gray' }}>Tambah Pasien</Text>
                            </Item>
                            <Item style={{ height: 50, paddingLeft: 10 }} onPress={this._handleLogout}>
                                <Icon name="exit" style={{ color: '#353E4F' }} />
                                <Text style={{ marginLeft: 10, color: 'gray' }}>Keluar</Text>
                            </Item>
                        </Container>
                    </View>
                }
            >
                <View style={[s.flx_i, s.aic, { backgroundColor: '#353E4F' }]}>
                    <View style={{
                        width,
                        height: 80,
                        backgroundColor: '#353E4F',
                        flexDirection: 'row',
                        borderBottomColor: colors.COOL_GREY_1,
                        borderBottomWidth: ms(1),
                    }}>
                        <View style={{ width: 50, height: 80 }}>
                            <Button
                                transparent
                                light
                                style={{ zIndex: 1, position: 'absolute', marginTop: '40%', marginLeft: -2 }}
                                onPress={this._handleOpenControlPanel}
                            >
                                <Icon style={{ color: 'white', fontSize: 30 }} name="menu" />
                            </Button>
                        </View>
                        <View style={{ width: width - 50, height: 80, paddingLeft: (width / 4.5), paddingTop: '8%' }}>
                            <Text style={{ color: 'white', fontSize: 18 }}>Dashboard</Text>
                        </View>
                    </View>

                    <View style={{ height: 10 }} />

                    <View style={[s.flx_i]}>
                        {
                            !_.isEmpty(this.state.data) ?
                                <FlatList
                                    data={this.state.data}
                                    extraData={this.props}
                                    initialNumToRender={7}
                                    keyExtractor={(item, index) => index.toFixed()}
                                    refreshControl={
                                        <RefreshControl
                                            onRefresh={this._handleGetDataPasien}
                                            refreshing={this.state.loading}
                                        />
                                    }
                                    renderItem={({ item }) => {
                                        return (
                                            <TouchableOpacity
                                                onPress={() => this._goToUkurPasien(item)}
                                            >
                                                <View style={{
                                                    marginVertical: ms(5),
                                                    paddingVertical: ms(5),
                                                    paddingHorizontal: ms(10),
                                                    width: width * 0.97,
                                                    backgroundColor: 'rgba(255,255,255,0.3)',
                                                }}>
                                                    <Text style={{ color: 'white', fontSize: ms(20), fontWeight: '300' }}>{item.nama}</Text>
                                                    <Text style={{ color: 'white', fontSize: ms(10) }}>{item.jenisKelamin}</Text>

                                                    <View style={[s.absolute, s.top_0, s.bottom_0, s.right_0, s.jcc, s.aic, {
                                                        marginRight: ms(10),
                                                    }]}>
                                                        <TouchableOpacity onPress={() => this._handleDeletePasien(item.id)}>
                                                            <View style={{ padding: ms(10) }}>
                                                                <Icon style={{ color: 'white' }} name="trash" />
                                                            </View>
                                                        </TouchableOpacity>
                                                    </View>
                                                </View>
                                            </TouchableOpacity>
                                        );
                                    }}
                                />

                                :

                                <ScrollView
                                    refreshControl={
                                        <RefreshControl
                                            onRefresh={this._handleGetDataPasien}
                                            refreshing={this.state.loading}
                                        />
                                    }
                                    contentContainerStyle={[s.flx_i, s.jcc, s.aic]}
                                >
                                    <Text style={{ color: '#FFF', fontSize: ms(16) }}>
                                        {'Tidak ada data pasien...'}
                                    </Text>
                                </ScrollView>
                        }
                    </View>
                </View>
            </Drawer>
        );
    }
}

Dashboard.propTypes = {
    navigation: PropTypes.shape().isRequired,
};

export default Dashboard;
