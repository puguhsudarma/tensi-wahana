import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    TextInput,
    Image,
    StatusBar,
    Dimensions,
    ActivityIndicator,
} from 'react-native';
import { StackActions, NavigationActions } from 'react-navigation';
import PropTypes from 'prop-types';
import { styles as s } from 'react-native-style-tachyons';
import LinearGradient from 'react-native-linear-gradient';
import { loginLogic } from '../logics/logic.account';
import { ms } from '../utils';

const { width } = Dimensions.get('window');

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            loading: false,
        };

        this._refPassword = null;
        this._handleLogin = this._handleLogin.bind(this);
        this._handleSignUp = this._handleSignUp.bind(this);
    }

    _handleLogin() {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: 'Dashboard' }),
            ],
        });
        loginLogic(
            this.state.username,
            this.state.password,
            () => this.props.navigation.dispatch(resetAction),
            () => this.setState({ loading: true }),
            () => this.setState({ loading: false }),
        );
    }

    _handleSignUp() {
        this.props.navigation.navigate('SignUp');
    }

    render() {
        return (
            <View style={[s.flx_i, s.aic, s.jcc, { backgroundColor: '#353E4F' }]}>
                <StatusBar
                    backgroundColor="rgba(16, 19, 22,0.05)"
                    translucent
                />
                <Image
                    source={require('../images/logo.png')}
                    style={{ height: 200, width: 200 }}
                />

                <TextInput
                    value={this.state.username}
                    onChangeText={username => this.setState({ username })}
                    placeholder="Username"
                    underlineColorAndroid="transparent"
                    editable={!this.state.loading}
                    onSubmitEditing={() => this._refPassword.focus()}
                    autoCapitalize="none"
                    returnKeyType="next"
                    style={[s.bg_white, {
                        textAlign: 'center',
                        width: width - 50,
                        height: 45,
                        borderRadius: 25,
                        marginTop: 20,
                    }]}
                />

                <TextInput
                    ref={(ref) => { this._refPassword = ref; }}
                    value={this.state.password}
                    onChangeText={password => this.setState({ password })}
                    placeholder="Password"
                    underlineColorAndroid="transparent"
                    editable={!this.state.loading}
                    secureTextEntry
                    autoCapitalize="none"
                    onSubmitEditing={this._handleLogin}
                    style={[s.bg_white, {
                        textAlign: 'center',
                        width: width - 50,
                        height: 45,
                        borderRadius: 25,
                        marginTop: 10,
                    }]}
                />

                <TouchableOpacity
                    style={{ marginTop: 20 }}
                    onPress={this._handleLogin}
                    disabled={this.state.loading}
                >
                    <LinearGradient
                        start={{ x: 0.0, y: 0.25 }}
                        end={{ x: 0.5, y: 1.0 }}
                        locations={[0, 0.6]}
                        colors={['#70D9A0', '#59C7E8']}
                        style={[s.aic, {
                            width: width - 50,
                            height: 45,
                            borderRadius: 25,
                            paddingTop: 8,
                        }]}
                    >
                        <Text style={{ fontSize: 20, color: 'black' }}>
                            {'Login'}
                        </Text>

                        {
                            this.state.loading &&
                            <View style={[s.absolute, s.top_0, s.bottom_0, s.right_0, s.jcc, s.aic, {
                                marginRight: ms(10),
                            }]}>
                                <ActivityIndicator color="#fff" size="large" />
                            </View>
                        }
                    </LinearGradient>
                </TouchableOpacity>

                <TouchableOpacity
                    onPress={this._handleSignUp}
                    disabled={this.state.loading}
                >
                    <Text style={{ color: 'white', fontSize: 14, marginTop: 20 }}>
                        {'Tidak Punya akun ? klik di sini'}
                    </Text>
                </TouchableOpacity>
            </View>

        );
    }
}

Login.propTypes = {
    navigation: PropTypes.shape().isRequired,
};

export default Login;
