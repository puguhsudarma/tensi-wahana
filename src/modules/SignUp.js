import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableOpacity,
    TextInput,
    StatusBar,
    Dimensions,
    ActivityIndicator,
    ScrollView,
} from 'react-native';
import PropTypes from 'prop-types';
import { styles as s } from 'react-native-style-tachyons';
import { Icon } from 'native-base';
import LinearGradient from 'react-native-linear-gradient';
import { signUpLogic } from '../logics/logic.account';
import { ms } from '../utils';

const { width } = Dimensions.get('window');

class SignUp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            nama: '',
            noTelepon: '',
            username: '',
            password: '',
            repassword: '',
            loading: false,
        };

        this._handleSignUp = this._handleSignUp.bind(this);
        this._handleBack = this._handleBack.bind(this);
    }

    _handleSignUp() {
        const { loading, ...data } = this.state;
        signUpLogic(
            data,
            () => this.props.navigation.navigate('Dashboard'),
            () => this.setState({ loading: true }),
            () => this.setState({ loading: false }),
        );
    }

    _handleBack() {
        this.props.navigation.goBack();
    }

    render() {
        return (
            <View style={[s.flx_i, { backgroundColor: '#353E4F' }]}>
                <StatusBar
                    backgroundColor="rgba(16, 19, 22,0.05)"
                    translucent
                />

                <View style={[s.flx_row, { width, height: 70 }]}>
                    <View style={{ marginTop: 30, marginLeft: 10 }}>
                        <TouchableOpacity
                            onPress={this._handleBack}
                            style={{ width: 20 }}
                        >
                            <Icon
                                name="ios-arrow-back"
                                style={{ color: '#fff', fontSize: 30 }}
                            />
                        </TouchableOpacity>
                    </View>

                    <View style={{ width: width - 80, marginTop: 30 }}>
                        <Text style={{ color: 'white', textAlign: 'center', fontSize: 18, marginTop: 5 }}>
                            {'DAFTAR'}
                        </Text>
                    </View>
                </View>

                <ScrollView keyboardShouldPersistTaps="handled">
                    <TextInput editable={!this.state.loading} value={this.state.nama} placeholder="nama" onChangeText={nama => this.setState({ nama })} underlineColorAndroid="transparent" style={{ textAlign: 'center', alignSelf: 'center', width: width - 50, height: 45, backgroundColor: 'white', borderRadius: 25, marginTop: 20 }} />
                    <TextInput editable={!this.state.loading} value={this.state.noTelepon} placeholder="No. Telepon" onChangeText={noTelepon => this.setState({ noTelepon })} underlineColorAndroid="transparent" style={{ textAlign: 'center', alignSelf: 'center', width: width - 50, height: 45, backgroundColor: 'white', borderRadius: 25, marginTop: 10 }} />
                    <TextInput editable={!this.state.loading} value={this.state.username} placeholder="Username" onChangeText={username => this.setState({ username })} underlineColorAndroid="transparent" style={{ textAlign: 'center', alignSelf: 'center', width: width - 50, height: 45, backgroundColor: 'white', borderRadius: 25, marginTop: 10 }} />
                    <TextInput editable={!this.state.loading} value={this.state.password} secureTextEntry placeholder="Password" onChangeText={password => this.setState({ password })} underlineColorAndroid="transparent" style={{ textAlign: 'center', alignSelf: 'center', width: width - 50, height: 45, backgroundColor: 'white', borderRadius: 25, marginTop: 10 }} />
                    <TextInput editable={!this.state.loading} value={this.state.repassword} secureTextEntry onSubmitEditing={this._handleSignUp} placeholder="Ulangi Password" onChangeText={repassword => this.setState({ repassword })} underlineColorAndroid="transparent" style={{ textAlign: 'center', alignSelf: 'center', width: width - 50, height: 45, backgroundColor: 'white', borderRadius: 25, marginTop: 10 }} />

                    <TouchableOpacity
                        disabled={this.state.loading}
                        style={{ marginTop: 20, alignSelf: 'center' }}
                        onPress={this._handleSignUp}
                    >
                        <LinearGradient
                            start={{ x: 0.0, y: 0.25 }}
                            end={{ x: 0.5, y: 1.0 }}
                            locations={[0, 0.6]}
                            colors={['#70D9A0', '#59C7E8']}
                            style={{ width: width - 50, height: 45, borderRadius: 25, alignItems: 'center', paddingTop: 8 }}
                        >
                            <Text style={{ fontSize: 20, color: 'black' }}>
                                {'Daftar'}
                            </Text>

                            {
                                this.state.loading &&
                                <View style={[s.absolute, s.top_0, s.bottom_0, s.right_0, s.jcc, s.aic, {
                                    marginRight: ms(10),
                                }]}>
                                    <ActivityIndicator color="#fff" size="large" />
                                </View>
                            }
                        </LinearGradient>
                    </TouchableOpacity>

                    <Text style={{ color: 'white', fontSize: 14, marginTop: 20, textAlign: 'center' }}>
                        {'Dengan Menekan Daftar Anda telah menyetujui persyaratan yang berlaku'}
                    </Text>
                </ScrollView>
            </View>
        );
    }
}

SignUp.propTypes = {
    navigation: PropTypes.shape().isRequired,
};

export default SignUp;
